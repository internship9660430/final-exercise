import {
  BadRequestException,
  Injectable,
  NotFoundException,
  NotImplementedException,
} from '@nestjs/common';
import * as bcrypt from 'bcrypt';
import { DatabaseService } from '@database';
import {
  CreateUserDto,
  UpdateUserDto,
  UserDto,
  UserWithoutPasswordDto,
} from '@shared-types';

@Injectable()
export class SharedUsersService {
  constructor(private readonly database: DatabaseService) {}

  async createUser(
    createUserDto: CreateUserDto
  ): Promise<UserWithoutPasswordDto | null> {
    if (await this.isEmailNotUnique(createUserDto.email)) {
      throw new BadRequestException('Email is not unique');
    }

    if (await this.isUsernameNotUnique(createUserDto.username)) {
      throw new BadRequestException('Username is not unique');
    }

    const user = await this.database.user.create({
      data: {
        ...createUserDto,
        password: await this.hashPassword(createUserDto.password),
      },
    });

    if (!user) throw new NotImplementedException('User not created');

    return this.removePassword(user);
  }

  async findAllUsers(): Promise<Array<UserWithoutPasswordDto> | null> {
    const users = await this.database.user.findMany();

    if (!users) return null;

    return users.map((user) => this.removePassword(user));
  }

  async findOneUser(id: number): Promise<UserWithoutPasswordDto | null> {
    const user = await this.database.user.findUnique({ where: { id } });

    if (!user) throw new NotFoundException(`User with id ${id} not found`);

    return this.removePassword(user);
  }

  async findOneUserForLogin(username: string): Promise<UserDto | null> {
    return await this.database.user.findUnique({ where: { username } });
  }

  async updateUser(
    id: number,
    updateUserDto: UpdateUserDto
  ): Promise<UserWithoutPasswordDto | null> {
    const existingUser = await this.database.user.findUnique({
      where: { id },
    });
    if (!existingUser)
      throw new NotFoundException(`User with id ${id} not found`);

    if (updateUserDto.email != null) {
      if (await this.isEmailNotUnique(updateUserDto.email)) {
        throw new BadRequestException('Email is not unique');
      }
    }

    if (updateUserDto.username != null) {
      if (await this.isUsernameNotUnique(updateUserDto.username)) {
        throw new BadRequestException('Username is not unique');
      }
    }

    if (updateUserDto.password != null) {
      updateUserDto.password = await this.hashPassword(updateUserDto.password);
    }

    const user = await this.database.user.update({
      where: { id },
      data: updateUserDto,
    });

    if (!user) throw new NotImplementedException('User not updated');

    return this.removePassword(user);
  }

  async removeUser(id: number): Promise<UserWithoutPasswordDto | null> {
    const existingUser = await this.database.user.findUnique({
      where: { id },
    });
    if (!existingUser)
      throw new NotFoundException(`User with id ${id} not found`);

    const user = await this.database.user.delete({ where: { id } });

    if (!user) throw new NotImplementedException('User not deleted');

    return this.removePassword(user);
  }

  private removePassword(userDto: UserDto): UserWithoutPasswordDto {
    const { password, ...userInfo } = userDto;
    return userInfo;
  }

  private async isEmailNotUnique(email: string): Promise<boolean> {
    const existingUser = await this.database.user.findUnique({
      where: { email },
    });
    return existingUser ? true : false;
  }

  private async isUsernameNotUnique(username: string): Promise<boolean> {
    const existingUser = await this.database.user.findUnique({
      where: { username },
    });
    return existingUser ? true : false;
  }

  private async hashPassword(password: string): Promise<string> {
    return await bcrypt.hash(password, 13);
  }
}
