import { Test, TestingModule } from '@nestjs/testing';
import { SharedUsersService } from './shared-users.service';

describe('SharedUsersService', () => {
  let service: SharedUsersService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [SharedUsersService],
    }).compile();

    service = module.get<SharedUsersService>(SharedUsersService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
