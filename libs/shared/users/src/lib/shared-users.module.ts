import { Module } from '@nestjs/common';
import { SharedUsersService } from './shared-users.service';
import { SharedTypesModule } from '@shared-types';
import { DatabaseModule } from '@database';

@Module({
  imports: [SharedTypesModule, DatabaseModule],
  providers: [SharedUsersService],
  exports: [SharedUsersService, SharedTypesModule],
})
export class SharedUsersModule {}
