import { PickType } from '@nestjs/swagger';
import { UserDto } from './user.dto';

export class AuthUserDto extends PickType(UserDto, ['id', 'role']) {}
