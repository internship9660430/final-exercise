import { OmitType } from '@nestjs/swagger';
import { ProductDto } from './product.dto';

export class ProductWithoutSellerIdDto extends OmitType(ProductDto, [
  'sellerId',
]) {}
