import { Module } from '@nestjs/common';
import { SharedProductsService } from './shared-products.service';
import { SharedTypesModule } from '@shared-types';
import { DatabaseModule } from '@database';

@Module({
  imports: [SharedTypesModule, DatabaseModule],
  providers: [SharedProductsService],
  exports: [SharedProductsService, SharedTypesModule],
})
export class SharedProductsModule {}
