import {
  BadRequestException,
  Injectable,
  NotFoundException,
  NotImplementedException,
} from '@nestjs/common';
import { DatabaseService } from '@database';
import {
  CreateProductDto,
  ProductDto,
  ProductWithoutSellerIdDto,
  UpdateProductDto,
} from '@shared-types';
import { Product } from '@prisma/client';
import { Decimal } from '@prisma/client/runtime/library';

@Injectable()
export class SharedProductsService {
  constructor(private readonly database: DatabaseService) {}

  async createProduct(
    createProductDto: CreateProductDto
  ): Promise<ProductDto | null> {
    if (await this.isSellerIdInvalid(createProductDto.sellerId)) {
      throw new BadRequestException('Seller id is not valid');
    }

    const product = await this.database.product.create({
      data: createProductDto,
    });

    if (!product) throw new NotImplementedException('Product not created');

    return this.convertDecimal(product);
  }

  async findAllProducts(): Promise<Array<ProductDto> | null> {
    const products = await this.database.product.findMany();

    if (!products) return null;

    return products.map((product) => this.convertDecimal(product));
  }

  async findProductsBySeller(
    sellerId: number
  ): Promise<Array<ProductWithoutSellerIdDto> | null> {
    const products = await this.database.product.findMany({
      where: { sellerId },
      select: {
        id: true,
        name: true,
        description: true,
        price: true,
        quantity: true,
      },
    });

    if (!products) return null;

    return products.map((product) => {
      return {
        ...product,
        price: new Decimal(product.price).toNumber(),
      };
    });
  }

  async findOneProduct(id: number): Promise<ProductDto | null> {
    const product = await this.database.product.findUnique({ where: { id } });

    if (!product)
      throw new NotFoundException(`Product with id ${id} not found`);

    return this.convertDecimal(product);
  }

  async updateProduct(
    id: number,
    updateProductDto: UpdateProductDto
  ): Promise<ProductDto | null> {
    const existingProduct = await this.database.product.findUnique({
      where: { id },
    });
    if (!existingProduct)
      throw new NotFoundException(`Product with id ${id} not found`);

    if (updateProductDto.sellerId != null) {
      if (await this.isSellerIdInvalid(updateProductDto.sellerId)) {
        throw new BadRequestException('Seller id is not valid');
      }
    }

    const product = await this.database.product.update({
      where: { id },
      data: updateProductDto,
    });

    if (!product) throw new NotImplementedException('Product not updated');

    return this.convertDecimal(product);
  }

  async removeProduct(id: number): Promise<ProductDto | null> {
    const existingProduct = await this.database.product.findUnique({
      where: { id },
    });
    if (!existingProduct)
      throw new NotFoundException(`Product with id ${id} not found`);

    const product = await this.database.product.delete({ where: { id } });

    if (!product) throw new NotImplementedException('Product not deleted');

    return this.convertDecimal(product);
  }

  private convertDecimal(productDto: Product): ProductDto {
    return {
      ...productDto,
      price: new Decimal(productDto.price).toNumber(),
    };
  }

  private async isSellerIdInvalid(id: number): Promise<boolean> {
    const seller = await this.database.user.findUnique({ where: { id } });
    return seller ? false : true;
  }
}
