import { Test } from '@nestjs/testing';
import { SharedProductsService } from './shared-products.service';

describe('SharedProductsService', () => {
  let service: SharedProductsService;

  beforeEach(async () => {
    const module = await Test.createTestingModule({
      providers: [SharedProductsService],
    }).compile();

    service = module.get(SharedProductsService);
  });

  it('should be defined', () => {
    expect(service).toBeTruthy();
  });
});
