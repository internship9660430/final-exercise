import {
  Injectable,
  NotFoundException,
  UnauthorizedException,
} from '@nestjs/common';
import { AuthUserDto, UserWithoutPasswordDto } from '@shared-types';
import { SharedUsersService } from '@shared-users';
import { JwtService } from '@nestjs/jwt';
import * as bcrypt from 'bcrypt';
import { Role } from '@prisma/client';

@Injectable()
export class AuthGuardService {
  constructor(
    private readonly usersService: SharedUsersService,
    private readonly jwtService: JwtService
  ) {}

  async validateUser(
    username: string,
    pass: string
  ): Promise<UserWithoutPasswordDto | null> {
    const user = await this.usersService.findOneUserForLogin(username);
    if (!user) return null;

    const isPasswordMatch = await bcrypt.compare(pass, user.password);
    if (!isPasswordMatch) return null;

    const { password, ...userWithoutPassword } = user;
    return userWithoutPassword;
  }

  async login(user: AuthUserDto) {
    return {
      access_token: this.jwtService.sign(user),
    };
  }

  async loginAdmin(user: AuthUserDto) {
    if (user.role != Role.ADMIN) throw new UnauthorizedException();
    return {
      access_token: this.jwtService.sign(user),
    };
  }

  async getUserInfo(payload: AuthUserDto) {
    const user = await this.usersService.findOneUser(payload.id);

    if (!user) throw new NotFoundException('Current user info not found');

    return user;
  }
}
