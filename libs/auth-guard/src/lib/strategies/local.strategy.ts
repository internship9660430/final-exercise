import { Injectable, UnauthorizedException } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { AuthUserDto } from '@shared-types';
import { Strategy } from 'passport-local';
import { AuthGuardService } from '../auth-guard.service';

@Injectable()
export class LocalStrategy extends PassportStrategy(Strategy) {
  constructor(private authGuardService: AuthGuardService) {
    super();
  }

  async validate(username: string, password: string): Promise<AuthUserDto> {
    const user = await this.authGuardService.validateUser(username, password);

    if (!user) throw new UnauthorizedException();

    return { id: user.id, role: user.role };
  }
}
