import { Module } from '@nestjs/common';
import { AuthGuardService } from './auth-guard.service';
import { SharedUsersModule } from '@shared-users';
import { PassportModule } from '@nestjs/passport';
import { LocalAuthGuard } from './guards/local-auth.guard';
import { LocalStrategy } from './strategies/local.strategy';
import { JwtModule } from '@nestjs/jwt';
import { JwtUserStrategy } from './strategies/jwt-user.strategy';
import { JwtAuthGuard } from './guards/jwt-auth.guard';
import { ConfigurationModule } from '@config';
import { ConfigService } from '@nestjs/config';
import { JwtAdminStrategy } from './strategies/jwt-admin.strategy';
import { JwtAdminGuard } from './guards/jwt-admin.guard';

@Module({
  imports: [
    SharedUsersModule,
    PassportModule,
    ConfigurationModule,
    JwtModule.registerAsync({
      imports: [ConfigurationModule],
      useFactory: (configService: ConfigService) => ({
        secret: configService.get<string>('JWT_SECRET_KEY'),
        signOptions: { expiresIn: '1h' },
      }),
      inject: [ConfigService],
    }),
  ],
  providers: [
    AuthGuardService,
    LocalStrategy,
    LocalAuthGuard,
    JwtUserStrategy,
    JwtAuthGuard,
    JwtAdminStrategy,
    JwtAdminGuard,
  ],
  exports: [AuthGuardService, LocalAuthGuard, JwtAuthGuard, JwtAdminGuard],
})
export class AuthGuardModule {}
