import { JwtAdminGuard } from '@auth-guard';
import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  ParseIntPipe,
  ValidationPipe,
  UseGuards,
} from '@nestjs/common';
import { ApiCreatedResponse, ApiOkResponse } from '@nestjs/swagger';
import { SharedProductsService } from '@shared-products';
import {
  CreateProductDto,
  ProductDto,
  UpdateProductDto,
  UserWithoutPasswordDto,
} from '@shared-types';
import { SharedUsersService } from '@shared-users';

@Controller('products')
@UseGuards(JwtAdminGuard)
export class ProductsController {
  constructor(
    private readonly productsService: SharedProductsService,
    private readonly usersService: SharedUsersService
  ) {}

  @Post()
  @ApiCreatedResponse({ type: ProductDto })
  createProduct(
    @Body(new ValidationPipe()) createProductDto: CreateProductDto
  ) {
    return this.productsService.createProduct(createProductDto);
  }

  @Get()
  @ApiOkResponse({ type: ProductDto, isArray: true })
  findAllProducts() {
    return this.productsService.findAllProducts();
  }

  @Get(':id')
  @ApiOkResponse({ type: ProductDto })
  findOneProduct(@Param('id', ParseIntPipe) id: number) {
    return this.productsService.findOneProduct(id);
  }

  @Patch(':id')
  @ApiCreatedResponse({ type: ProductDto })
  updateProduct(
    @Param('id', ParseIntPipe) id: number,
    @Body(new ValidationPipe()) updateProductDto: UpdateProductDto
  ) {
    return this.productsService.updateProduct(id, updateProductDto);
  }

  @Delete(':id')
  @ApiOkResponse({ type: ProductDto })
  removeProduct(@Param('id', ParseIntPipe) id: number) {
    return this.productsService.removeProduct(id);
  }

  @Get(':id/seller')
  @ApiOkResponse({ type: UserWithoutPasswordDto })
  async findOneProductSellerInfo(@Param('id', ParseIntPipe) id: number) {
    const product = await this.productsService.findOneProduct(id);
    return this.usersService.findOneUser(product.sellerId);
  }
}
