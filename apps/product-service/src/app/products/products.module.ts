import { Module } from '@nestjs/common';
import { ProductsController } from './products.controller';
import { SharedProductsModule } from '@shared-products';
import { AuthGuardModule } from '@auth-guard';
import { SharedUsersModule } from '@shared-users';

@Module({
  imports: [SharedProductsModule, SharedUsersModule, AuthGuardModule],
  controllers: [ProductsController],
})
export class ProductsModule {}
