import { Module } from '@nestjs/common';
import { UsersController } from './users.controller';
import { SharedUsersModule } from '@shared-users';
import { AuthGuardModule } from '@auth-guard';
import { SharedProductsModule } from '@shared-products';

@Module({
  imports: [SharedUsersModule, SharedProductsModule, AuthGuardModule],
  controllers: [UsersController],
})
export class UsersModule {}
