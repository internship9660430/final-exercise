import { Request as ExpressRequest } from 'express';
import { AuthGuardService, JwtAdminGuard } from '@auth-guard';
import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  ParseIntPipe,
  ValidationPipe,
  UseGuards,
  Request,
  UnauthorizedException,
} from '@nestjs/common';
import { ApiCreatedResponse, ApiOkResponse } from '@nestjs/swagger';
import {
  AuthUserDto,
  CreateUserDto,
  ProductWithoutSellerIdDto,
  UpdateUserDto,
  UserWithoutPasswordDto,
} from '@shared-types';
import { SharedUsersService } from '@shared-users';
import { Role } from '@prisma/client';
import { SharedProductsService } from '@shared-products';

@Controller('users')
@UseGuards(JwtAdminGuard)
export class UsersController {
  constructor(
    private readonly usersService: SharedUsersService,
    private readonly productsService: SharedProductsService,
    private authGuardService: AuthGuardService
  ) {}

  @Post()
  @ApiCreatedResponse({ type: UserWithoutPasswordDto })
  createUser(@Body(new ValidationPipe()) createUserDto: CreateUserDto) {
    return this.usersService.createUser(createUserDto);
  }

  @Get()
  @ApiOkResponse({ type: UserWithoutPasswordDto, isArray: true })
  async findAllUsers(@Request() req: ExpressRequest) {
    const user = req.user as AuthUserDto;

    if (user.role == Role.ADMIN) return this.usersService.findAllUsers();

    throw new UnauthorizedException('Only admins can access this');
  }

  @Get(':id')
  @ApiOkResponse({ type: UserWithoutPasswordDto })
  findOneUser(@Param('id', ParseIntPipe) id: number) {
    return this.usersService.findOneUser(id);
  }

  @Patch(':id')
  @ApiCreatedResponse({ type: UserWithoutPasswordDto })
  updateUser(
    @Param('id', ParseIntPipe) id: number,
    @Body(new ValidationPipe()) updateUserDto: UpdateUserDto
  ) {
    return this.usersService.updateUser(id, updateUserDto);
  }

  @Delete(':id')
  @ApiOkResponse({ type: UserWithoutPasswordDto })
  removeUser(@Param('id', ParseIntPipe) id: number) {
    return this.usersService.removeUser(id);
  }

  @Get(':id/products')
  @ApiOkResponse({ type: ProductWithoutSellerIdDto, isArray: true })
  async findOneUserProducts(@Param('id', ParseIntPipe) id: number) {
    await this.usersService.findOneUser(id);
    return this.productsService.findProductsBySeller(id);
  }
}
