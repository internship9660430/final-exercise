import { Module } from '@nestjs/common';
import { AuthController } from './auth.controller';
import { AuthGuardModule } from '@auth-guard';
import { SharedTypesModule } from '@shared-types';

@Module({
  imports: [AuthGuardModule, SharedTypesModule],
  controllers: [AuthController],
})
export class AuthModule {}
