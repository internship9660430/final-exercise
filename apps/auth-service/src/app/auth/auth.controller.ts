import { Request as ExpressRequest } from 'express';
import { AuthGuardService, JwtAuthGuard, LocalAuthGuard } from '@auth-guard';
import { Controller, Get, Post, UseGuards, Request } from '@nestjs/common';
import { ApiBearerAuth, ApiBody, ApiResponse } from '@nestjs/swagger';
import {
  AuthUserDto,
  LoginUserDto,
  TokenDto,
  UserWithoutPasswordDto,
} from '@shared-types';

@Controller('auth')
export class AuthController {
  constructor(private readonly authGuardService: AuthGuardService) {}

  @Post('login')
  @UseGuards(LocalAuthGuard)
  @ApiBody({ type: LoginUserDto })
  @ApiResponse({ type: TokenDto })
  async login(@Request() req: ExpressRequest) {
    return this.authGuardService.login(req.user as AuthUserDto);
  }

  @Post('login/admin')
  @UseGuards(LocalAuthGuard)
  @ApiBody({ type: LoginUserDto })
  @ApiResponse({ type: TokenDto })
  async loginAdmin(@Request() req: ExpressRequest) {
    return this.authGuardService.loginAdmin(req.user as AuthUserDto);
  }

  @Get('current-user')
  @UseGuards(JwtAuthGuard)
  @ApiBearerAuth('JWT-auth')
  @ApiResponse({ type: UserWithoutPasswordDto })
  getCurrentUser(@Request() req: ExpressRequest) {
    return this.authGuardService.getUserInfo(req.user as AuthUserDto);
  }
}
